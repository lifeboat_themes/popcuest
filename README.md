# Popcuest
A Light colour based theme which is suitable for shops with numerous collections.

## Custom Fields

### Banner Collections
You can select up to 3 collections to be featured in the home page banner. One collection in each custom field:

Home: Banner Collection 1, Home: Banner Collection 2, Home: Banner Collection 3.

### Banner Heading
You can have a custom banner heading to be shown in the home page, if the collection is selected to be shown in the banner.

### Featured Collections
You can have 3 collections to be shown under the banner in the home page.
This is done by selecting a collection in the custom fields

Home: Featured Collection 1, Home: Featured Collection 2, Home: Featured Collection 3.

### Product Carousels
You can select up to 3 collections to be featured in the home page, showing various products of the collection. One Collection in each custom field

Home: Product Carousel 1, Home: Product Carousel 2, Home: Product Carousel 3.

Product Carousel 1 is shown under the Featured Collections
Product Carousel 2 is shown in the middle of the home page
Product Carousel 3 is shown just above the footer

### Collections Carousel
This section enables you to have a carousel of collections to direct users to your preferred collections.

This is done by adding the Tag 'featured' in the tags section when editing a Collection.

### Shop by collections Section Title
You can customise the title for the 'Shop By Collections' section, where the default title is 'Our Collections'.

### Collection Cards
Under the Collections Carousel you may also add 2 more collections which span the full width of the page to attract attention.

These can be set in the custom fields 'Collection Card Left' and 'Collection Card Right'.

### Video section image/thumbnail
This image will be displayed as a thumbnail for an optional video.

### Video section video youtube link
This text field takes a video link for an optional YouTube video to be shown on the home page.

### Video section Title
This text will be displayed as a Title on the video section. 

### Video section Subtitle
This text will be displayed as a subtitle on the video section, above the title.

### Footer: First Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown on the left of the three menu options.

_Note: Submenus items will not be displayed_

### Footer: Second Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown in the middle of the three menu options.

_Note: Submenus items will not be displayed_

### Footer: Third Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown on the right of the three menu options.

_Note: Submenus items will not be displayed_

### Category
You may add a category to each Product, as to improve user experience on your Lifeboat store.
This is done by selecting a collection in the field labelled 'Category' in the Product of your choice.

### Product Page promoted Collection
You can select a collection to be featured on the right in a sidebar when viewing a single product.

### Product Page promoted items
You can select a collection to have 5 products from that collection featured in the sidebar when viewing a single product.

### Featured Items Section Title
You can customise the title for the featured items section, where the default title is 'Featured Items'.
This is displayed in the product page on the right sidebar.

## Collection: Custom Fields

### Banner Heading
This text will be displayed on the banner above the title, if the collection is selected as a banner collection.

### Collection Subtitle
This text will be displayed where a collection subtitle is supported, where a collection is selected to appear in a section.

### Collection Banner
You can select an image to feature in the banner when viewing a single collection. If left empty, the default collection image is shown.