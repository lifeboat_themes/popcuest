<div class="col-lg-4 col-sm-6">
    <div class="banner banner-{$order} banner-fixed content-middle <% if order == 2 %> content-center <% end_if %> overlay-dark appear-animate"
         data-animation-options="{
                            'name': 'fadeInLeftShorter',
                            'delay': '.5s'
                        }">
        <figure>
            <img src="$Collection.Image.Fill(380, 207).AbsoluteURL" alt="$Collection.Title" width="380" height="207"
                 style="background-color: rgb(233, 233, 233);" />
        </figure>
        <div class="banner-content">
            <h3 class="banner-title font-weight-bold">$Collection.Title</h3>
            <% if $Collection.CustomField('CollectionSubtitle') %>
                <h4 class="banner-subtitle text-uppercase text-body mb-0">
                    $Collection.CustomField('CollectionSubtitle')
                </h4>
            <% end_if %>
            <hr class="bg-grey">
            <a href="$Collection.AbsoluteLink" class="btn btn-link btn-underline">Shop Now<i class="d-icon-arrow-right"> </i> </a>
        </div>
    </div>
</div>