<footer class="footer appear-animate">
    <div class="footer-middle">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="widget widget-about">
                        <div class="footer-title pt-0 mt-0">
                            $SiteSettings.Title
                        </div>
                        <div class="widget-body">
                            <% if $ContactPage.Address %>
                                    <label class="mt-3">Address</label>
                                    <span>$ContactPage.Address.RAW</span>
                                <% end_if %>

                                <% if $ContactPage.Email %>
                                    <label class="mt-3">Email</label>
                                    <a href="mailto:$ContactPage.Email">$ContactPage.Email</a><br/>
                                <% end_if %>

                                <% if $ContactPage.Tel %>
                                    <label class="mt-3">Phone</label>
                                    <span>$ContactPage.Tel</span><br/>
                                <% end_if %>
                        </div>
                    </div>
                </div>
                <% if $Theme.CustomField('FooterMenu1').Value() %>
                    <div class="col-xl-2 col-lg-4 col-sm-6">
                        <div class="widget">
                            <h4 class="widget-title">$Theme.CustomField('FooterMenu1').Value.Title</h4>
                            <ul class="widget-body">
                                <% loop $Theme.CustomField('FooterMenu1').Value.MenuItems %>
                                    <li>
                                        <a href="$Link">$Title</a>
                                    </li>
                                <% end_loop %>
                            </ul>
                        </div>
                    </div>
                <% end_if %>
                <% if $Theme.CustomField('FooterMenu2').Value() %>
                    <div class="col-xl-2 col-lg-4 col-md-6">
                        <div class="widget">
                            <h4 class="widget-title">$Theme.CustomField('FooterMenu2').Value.Title</h4>
                            <ul class="widget-body">
                                <% loop $Theme.CustomField('FooterMenu2').Value.MenuItems %>
                                    <li>
                                        <a href="$Link">$Title</a>
                                    </li>
                                <% end_loop %>
                            </ul>
                        </div>
                    </div>
                <% end_if %>
                <% if $Theme.CustomField('FooterMenu3').Value() %>
                    <div class="col-xl-2 col-lg-4 col-md-6">
                        <div class="widget">
                            <h4 class="widget-title">$Theme.CustomField('FooterMenu3').Value.Title</h4>
                            <ul class="widget-body">
                                <% loop $Theme.CustomField('FooterMenu3').Value.MenuItems %>
                                    <li>
                                        <a href="$Link">$Title</a>
                                    </li>
                                <% end_loop %>
                            </ul>
                        </div>
                    </div>
                <% end_if %>
                <div class="col-xl-3 col-lg-8">
                    <% if $Integration('Mailchimp').isEnabled %>
                    <div class="widget">
                        <div class="alert mb-md-4" id="subscribe-alert" style="display:none;"></div>
                        <h4 class="widget-title pt-1 mb-4">Subscribe to Our Newsletter</h4>
                        <div class="widget-body widget-newsletter pt-0">
                            <form class="input-wrapper input-wrapper-inline mailchimp-subscribe" data-alert="#subscribe-alert">
                                <input type="email" class="form-control" name="email" id="email"
                                       placeholder="Email address here..." required />
                                <button class="btn btn-primary btn-sm btn-icon-right font-primary"
                                        type="submit">subscribe<i class="d-icon-arrow-right"></i></button>
                            </form>
                        </div>
                    </div>
                    <% end_if %>
                    <div class="footer-info d-flex align-items-center justify-content-between">
                        <% if $SocialLinks.Count %>
                            <div class="social-links">
                                <% loop $SocialLinks %>
                                    <a href="$URL" target="_blank" class="social-link social-$Site.LowerCase fab fa-$Site.LowerCase"> </a>
                                <% end_loop %>
                            </div>
                        <% end_if %>
                    </div>
                    <!-- End of Widget -->
                </div>
            </div>
        </div>
    </div>
    <!-- End of FooterMiddle -->
    <div class="footer-bottom d-block">
        <div class="container-fluid justify-content-center">
            <div class="footer-left">
                <p class="copyright">Web-shop built using <a href="https://lifeboat.app" target="_blank" rel="noopener"> Lifeboat</a></p>
            </div>
            <div class="footer-center mb-0">
                <p class="copyright">© $Now.Format('Y') <a href="/">{$SiteSettings.Title}.</a> All rights reserved.</p>
            </div>
        </div>
    </div>
    <!-- End of FooterBottom -->
</footer>